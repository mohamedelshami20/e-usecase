variable "MASTER_PORT" {
  default = "8080"
}

variable "AWS_REGION" {
  default = "us-east-1"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "/home/h4ckr007/.aws/mykey"
}
variable "PATH_TO_PUBLIC_KEY" {
  default = "/home/h4ckr007/.aws/mykey.pub"
}

variable "PATH_TO_EC2_PLUGIN_PUBLIC_KEY" {
  default = "/home/h4ckr007/.aws/rsa.pub"
}

variable "INSTANCE_USERNAME" {
  default = "ubuntu"
}

variable "JENKINS_VERSION" {
  default = "2.190.3"
}

variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}

variable "JENKINS_ADMIN_PASS" {
  default = "Sup3rP"
}

variable "ENV_TYPE" {
  default = "dev"
}

