pipeline {
    agent any
    stages{
        stage ('Checkout') {
            steps {
                git url: "${gitlabSourceRepoHttpUrl}", credentialsId: '1706b422-7e64-4708-a57c-76ed409da939', branch: '${gitlabBranch}'
                script {
                    env.commitId = env.gitlabAfter.substring(0,10)
                }
            }

        }
        stage('Build Branch') {
            steps {
                script{
                    buildInfo = rtMaven.run pom: './pom.xml', goals: 'clean install -DskipDockerBuild -Dmaven.test.skip=true'
                    def pom = readMavenPom file:'pom.xml'
                    print pom.version
                    env.version = pom.version
                    env.serviceName = pom.artifactId
                }
            }
        }

        stage('Run tests') {
            steps {
                script{
                    checkUrl = "testReport/"
                    buildInfo = rtMaven.run pom: './pom.xml', goals: 'test -Dmaven.test.failure.ignore=true'
                    print "Build info --> " + buildInfo
                }
            }
        }

        stage('SonarQube analysis') {
            withSonarQubeEnv(credentialsId: 'someID', installationName: 'SonarQube-Server') {
                sh 'mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.6.0.1398:sonar'
            }
        }   
        stage("Quality Gate") {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    // This fails the pipeline if the quality gate did not pass
                    waitForQualityGate abortPipeline: true
                }
            }
        }

        stage('Build Image') {
            steps {
                script {
                def pom = readMavenPom file:'pom.xml'
                dir ('target') {
                        sh 'cp ../src/main/docker/* .'
                        app = docker.build ("registry.dev:443/${gitGroup}/${env.serviceName}:${env.commitId}", "--build-arg JARFILE=${env.serviceName}-${env.version}.jar ./")
                    } 
                }
            }
        }

        stage('Publish to Docker registry') {
            steps {
                script{
                    docker.withRegistry('http://registry.dev:443', 'DevRegistryCreds') {
                        app.push()
                    }
                }
            }
        }

        // Assuming the app is deployed on k8s cluster, and the registry is configured with k8s
        stage ('Deploy'){
            steps {
                script {
                    // Need to update the tag in the chart with this commit

                    // Upgrade release
                    sh 'helm upgrade --install  app-release ./charts/app'
                }
            }
        }

    post {
        success {
            script{
                message = "SUCCESS"
                notifyBuild("SUCCESS", "", "${message}", "")
                cleanup()
            }
        }
        failure {
            script{
                handleFailure()
            }
        }
        unstable{
            script{
                message = "Test Failure"
                notifyBuild("TEST_FAILURE", "", "${message}", "testReport/")
                cleanup()
            }
        }
        always {
            echo 'Always'
            cleanWs()
        }
    }
}

def cleanup(){
   echo "Clean up Function"
   // Do Any cleanup required
}

def handleFailure(){
    // Handle Failures 
    notifyBuild("FAILURE", mergeResult , "Failure", "console")
    cleanup()
}

// Send Notifications to Slack about Build status
def notifyBuild(String buildStatus = 'STARTED', String message = '', String result = '', String checkUrl = '') {
  echo "NOTIFY BUILD"
  buildStatus =  buildStatus ?: 'SUCCESS'

  // Default values
  def colorName = 'RED'
  def colorCode = '#FF0000'
  def subject = "${buildStatus}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'"
  def summary = "${subject} (${env.BUILD_URL}${checkUrl})"

  // Override default values based on build status
  if (buildStatus.contains('TEST_FAILURE')) {
    color = 'YELLOW'
    colorCode = '#FFFF00'
  } else if (buildStatus.contains('SUCCESS')) {
    color = 'GREEN'
    colorCode = '#00FF00'
  } else {
    color = 'RED'
    colorCode = '#FF0000'
  }

  // Send notifications / Assuming slack is propery configured.
  slackSend (color: colorCode, message: summary + message + " "+result)

}
