#!/bin/bash

# volume setup
vgchange -ay

DEVICE_FS=`blkid -o value -s TYPE ${DEVICE}`
if [ "`echo -n $DEVICE_FS`" == "" ] ; then
  # wait for the device to be attached
  DEVICENAME=`echo "${DEVICE}" | awk -F '/' '{print $3}'`
  DEVICEEXISTS=''
  while [[ -z $DEVICEEXISTS ]]; do
    echo "checking $DEVICENAME"
    DEVICEEXISTS=`lsblk |grep "$DEVICENAME" |wc -l`
    if [[ $DEVICEEXISTS != "1" ]]; then
      sleep 15
    fi
  done
  pvcreate ${DEVICE}
  vgcreate data ${DEVICE}
  lvcreate --name volume1 -l 100%FREE data
  mkfs.ext4 /dev/data/volume1
fi
mkdir -p /var/lib/jenkins
echo '/dev/data/volume1 /var/lib/jenkins ext4 defaults 0 0' >> /etc/fstab
mount /var/lib/jenkins

apt-get update


# install pip
wget -q https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
rm -f get-pip.py
# install awscli
pip install awscli

## install ansible
pip install ansible



# Install Jenkins through ansible role
cd /tmp && git clone https://github.com/geerlingguy/ansible-role-jenkins.git
ansible-galaxy install geerlingguy.jenkins
cat > playbook.yaml << EOF
- hosts: localhost
  connection: local
  become: yes
  vars:
    jenkins_hostname: localhost
    jenkins_admin_password: ${JENKINS_ADMIN_PASS}
    jenkins_plugins:
      - git-client 
      - git 
      - github-api 
      - github-oauth 
      - github 
      - MSBuild 
      - ssh-slaves 
      - workflow-aggregator 
      - ws-cleanup
      - ec2
      - build-monitor
      - configuration-as-code
      - ssh-credentials
  roles:
    - ansible-role-jenkins
EOF
ansible-playbook playbook.yaml

