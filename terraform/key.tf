resource "aws_key_pair" "jkz-master" {
  key_name = "jkz-master"
  public_key = "${file("${var.PATH_TO_PUBLIC_KEY}")}"
  lifecycle {
    ignore_changes = [public_key]
  }
}

resource "aws_key_pair" "jkz-ec2-plugin" {
  key_name = "jkz-ec2-plugin"
  public_key = "${file("${var.PATH_TO_EC2_PLUGIN_PUBLIC_KEY}")}"
  lifecycle {
    ignore_changes = [public_key]
  }
}

