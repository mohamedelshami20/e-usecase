resource "aws_iam_role" "Jenkins-iam-ec2-role" {
  name = "Jenkins-iam-ec2-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "Jenkins-iam-role-instanceprofile" {
  name = "Jenkins-iam-role-profile"
  role = "${aws_iam_role.Jenkins-iam-ec2-role.name}"
}

resource "aws_iam_role_policy" "Jenkins-iam-role-policy" {
  name = "Jenkins-iam-role-policy"
  role = "${aws_iam_role.Jenkins-iam-ec2-role.id}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action":[
                "ec2:*",
                "iam:*",
"elasticloadbalancing:*",
"autoscaling:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

