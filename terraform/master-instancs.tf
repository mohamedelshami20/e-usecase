provider "aws" {
  region = "${var.AWS_REGION}"
}


resource "aws_instance" "jkz-master-1" {
  ami           = "${data.aws_ami.latest-ubuntu.id}"
  instance_type = "t2.micro"

  # the VPC subnet
  subnet_id = "${aws_subnet.main-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.jkz-master-securitygroup.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.jkz-master.key_name}"

  # user data
  user_data = "${data.template_cloudinit_config.cloudinit-jenkins.rendered}"

  iam_instance_profile = "${aws_iam_instance_profile.Jenkins-iam-role-instanceprofile.name}"

  tags = {
    env = "${var.ENV_TYPE}"
    name = "jkz-master-1"
  }

}

resource "aws_ebs_volume" "jkz-master-1-data" {
  availability_zone = "us-east-1a"
  size = 10
  type = "gp2"
  tags = {
    Name = "jkz-master-1-data"
    env = "${var.ENV_TYPE}"
  }
}

resource "aws_volume_attachment" "jkz-master-1-data-attachment" {
  device_name = "${var.INSTANCE_DEVICE_NAME}"
  volume_id = "${aws_ebs_volume.jkz-master-1-data.id}"
  instance_id = "${aws_instance.jkz-master-1.id}"
  skip_destroy = true
}

output "jenkins-ip" {
  value = ["${aws_instance.jkz-master-1.*.public_ip}"]
}
