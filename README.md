# AWS Jenkins Cluster 

## Usage 

1. Make sure your are exporting your AWS access_keys before running the scripts.
2. Run `terraform init` to initialize required modules.
3. Run `terrform plan` to plan the change that will be done by terraform and double check it.
4. Run `terraform apply`
5. You need to connect to the masters and configure the amazon-ec2 plugin

## Notes:
1. There were some better options like using Packer to bake images that is ready with all tools and configurations required.
2. Use configuration as code for configuring jenskins and it's plugins.
3. In the architecture diagram, I have mentioned that I am provisioning two masters, and loadbalancing using ELB. I did not add it to terraform since it's straight forward.
4. I am using the build-monitor plugin for showing the build status of the jobs.
5. Should have added SSL for the ELB.
